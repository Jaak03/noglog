"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _winston = require("winston");

require("winston-daily-rotate-file");

var _makeOptions = _interopRequireDefault(require("./helpers/makeOptions"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var combine = _winston.format.combine,
    timestamp = _winston.format.timestamp,
    label = _winston.format.label,
    prettyPrint = _winston.format.prettyPrint,
    errors = _winston.format.errors;
/**
 * Default winston logger to be expanded in the future.
 *
 * @param service label to show where the log is coming from.
 * @returns winston logger.
 */

var logger = function logger() {
  var service = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'general';
  var config = arguments.length > 1 ? arguments[1] : undefined;
  var options = (0, _makeOptions["default"])(_winston.format, config);
  return function () {
    var message = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
    var level = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'info';
    return (0, _winston.createLogger)({
      level: process.env.LOGGING_LEVEL || 'info',
      levels: options.loggingLevels,
      handleExceptions: true,
      json: false,
      colorize: true,
      format: combine(errors({
        stack: true
      }), label({
        label: service
      }), timestamp(), prettyPrint()),
      transports: [new _winston.transports.Console(), new _winston.transports.DailyRotateFile(options.dailyRotateFile)]
    }).log(level, message);
  };
}; // Catching very bad exceptions.


process.on('uncaughtException', function (err) {
  logger('process')("NODE CRASH: ".concat(new Date().toUTCString(), " uncaughtException: ").concat(JSON.stringify(err)), 'error');
  process.exit(1);
});
var _default = logger;
exports["default"] = _default;
//# sourceMappingURL=logger.js.map