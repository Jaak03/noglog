"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _default2 = _interopRequireDefault(require("../config/default"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

/**
 * https://github.com/winstonjs/winston#creating-your-own-logger
 * Returns a configuration object to be used in creating the logger.
 * @param format format value that is part of the winston module.
 * @param configuration javascript object that contains options for setting up the logger.
 */
function makeOptions(format) {
  var configuration = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : _default2["default"];
  var combine = format.combine,
      label = format.label,
      json = format.json,
      timestamp = format.timestamp;

  var application = configuration.application,
      cfg = _objectWithoutProperties(configuration, ["application"]);

  return {
    dailyRotateFile: _objectSpread({
      filename: "".concat(application, "-[%DATE%].log"),
      format: combine(label({
        label: application
      }), json(), timestamp())
    }, cfg.dailyRotateFile),
    loggingLevels: {
      error: 0,
      warn: 1,
      info: 2,
      debug: 3
    }
  };
}

var _default = makeOptions;
exports["default"] = _default;
//# sourceMappingURL=makeOptions.js.map