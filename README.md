# Noglog

An easy to use Winston logger that I have configured to use in my projects.

## To use ##
The namespace is the label that the logs are made for. To use the logger you import it and assign a namespace to the logger before you send a string message and a string log level based on the ones you set in the config file.

If you are planning to use a single namespace throughout your file, you can go ahead and set that during the require step:

    const log = require('noglog')('test-file');

    log('test');

The other option is to have a namespace for every function and in this case you will do the following:

    const logger = require('noglog')

    ...

    function test() {
      const log = logger('test');
    }

### Configuration file ###
There is a default configuration and you can use the logger straight out of the box without setting anything up, but if you would like to customize a few things here is an example of what it should look like at this point:

    module.exports = {
      loglevels: {
        error: 0,
        warn: 1,
        info: 2,
        debug: 3,
      },
      dailyRotateFile: {
        dirname: './logs',
        datePattern: 'YYYY-MM-DD',
        zippedArchive: true,
        maxSize: '20m',
        level: 'info',
        handleExceptions: true,
        json: true,
      },
      application: 'application',
    };

## Dependencies ##
This is using the [Winston](https://github.com/winstonjs/winston) logger, so the dependencies are:
* winston
* winston-daily-rotate-file

## Issues & improvements ##
If you find any issues, feel free to open one in the repo or make a PR. I'm sure there are things that I have done incorrectly, so if you are aching to fix something don't even thing twice about it and PR me immediately ;-)... 