import defaultCfg from '../config/default';

/**
 * https://github.com/winstonjs/winston#creating-your-own-logger
 * Returns a configuration object to be used in creating the logger.
 * @param format format value that is part of the winston module.
 * @param configuration javascript object that contains options for setting up the logger.
 */
function makeOptions(format, configuration = defaultCfg) {
  const {
    combine,
    label,
    json,
    timestamp,
  } = format;

  const { application, ...cfg } = configuration;

  return {
    dailyRotateFile: {
      filename: `${application}-[%DATE%].log`,
      format: combine(
        label({
          label: application,
        }),
        json(),
        timestamp(),
      ),
      ...cfg.dailyRotateFile,
    },
    loggingLevels: {
      error: 0,
      warn: 1,
      info: 2,
      debug: 3,
    },
  };
}

export default makeOptions;
