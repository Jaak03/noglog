export default ({
  // https://github.com/winstonjs/winston#creating-your-own-logger
  loglevels: {
    error: 0,
    warn: 1,
    info: 2,
    debug: 3,
  },
  dailyRotateFile: {
    dirname: './logs',
    datePattern: 'YYYY-MM-DD',
    zippedArchive: true,
    maxSize: '20m',
    level: 'info',
    handleExceptions: true,
    json: true,
  },
  application: 'application',
});
