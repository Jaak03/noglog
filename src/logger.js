import { createLogger, format, transports } from 'winston';
import 'winston-daily-rotate-file';

import makeOptions from './helpers/makeOptions';

const {
  combine,
  timestamp,
  label,
  prettyPrint,
  errors,
} = format;

/**
 * Default winston logger to be expanded in the future.
 *
 * @param service label to show where the log is coming from.
 * @returns winston logger.
 */
const logger = (
  service = 'general',
  config,
) => {
  const options = makeOptions(format, config);

  return (
    message = '',
    level = 'info',
  ) => createLogger({
    level: process.env.LOGGING_LEVEL || 'info',
    levels: options.loggingLevels,
    handleExceptions: true,
    json: false,
    colorize: true,
    format: combine(
      errors({ stack: true }),
      label({ label: service }),
      timestamp(),
      prettyPrint(),
    ),
    transports: [
      new transports.Console(),
      new transports.DailyRotateFile(options.dailyRotateFile),
    ],
  }).log(level, message);
};

// Catching very bad exceptions.
process.on('uncaughtException', (err) => {
  logger('process')(
    `NODE CRASH: ${(new Date()).toUTCString()} uncaughtException: ${JSON.stringify(err)}`,
    'error',
  );
  process.exit(1);
});

export default logger;
